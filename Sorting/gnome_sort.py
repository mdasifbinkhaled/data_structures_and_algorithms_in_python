def gnome_sort(arr, n):

    i = 0

    while i < n:

        if i == 0 or arr[i] >= arr[i - 1]:

            i = i + 1

        else:

            arr[i], arr[i - 1] = arr[i - 1], arr[i]

            i = i - 1

    return arr

array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = gnome_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))