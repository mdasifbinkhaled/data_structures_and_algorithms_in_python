import numpy as np

def cycle_sort(arr, n):

    writes = 0

    for i in range(0, n - 1):

        current_item = arr[i]

        target_position = i

        for j in range(i+1, n):

            if current_item>arr[j]:

                target_position = target_position + 1

        if target_position == i:
            
            continue

        while current_item == arr[target_position]:

            target_position = target_position + 1

        arr[target_position], current_item = current_item, arr[target_position]

        writes = writes + 1

        while target_position != i:

            target_position = i

            for j in range(i+1, n):

                if arr[j]<current_item:

                    target_position = target_position + 1

            while current_item == arr[target_position]:

                target_position = target_position + 1

            arr[target_position], current_item = current_item, arr[target_position]

            writes = writes + 1

    return arr

array = np.array([2, 1, 123, 2, 1, 2,0, 3, 4,0, 12, 3, 12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = cycle_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))