import numpy as np

def counting_sort_with_negative_integers(arr, n, l, u):

    k = u - l + 1

    output_arr = np.zeros(n, dtype=int)

    frequency_arr = np.zeros(k, dtype=int)

    for i in range(n):

        frequency_arr[arr[i] - l] = frequency_arr[arr[i] - l] + 1

    for i in range(1, k):

        frequency_arr[i] = frequency_arr[i] + frequency_arr[i - 1]

    for i in range(n-1, -1, -1):

        output_arr[frequency_arr[arr[i] - l] - 1] = arr[i]

        frequency_arr[arr[i] - l] = frequency_arr[arr[i] - l] - 1

    return output_arr

array = np.array([2, 1, 2, 1, -2, 0, 3, 4, 0, 12, 3, -12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = counting_sort_with_negative_integers(array, len(array), min(array), max(array))

print('Output Array {0}'.format(sorted_array))
