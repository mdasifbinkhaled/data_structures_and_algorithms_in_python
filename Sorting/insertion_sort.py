def insertion_sort(arr, n):

    for i in range(1, n):

        key = arr[i]

        j = i - 1

        while j >= 0 and arr[j] > key:

            arr[j + 1] = arr[j]

            j = j - 1

        arr[j + 1] = key

    return arr


array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = insertion_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))
