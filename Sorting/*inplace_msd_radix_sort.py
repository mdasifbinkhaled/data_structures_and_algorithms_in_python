import numpy as np


def inplace_msd_radix_sort(arr, n, exponent, base):

    if n <= 1:
        return arr

    frequency_arr = np.zeros(base, dtype=int)

    for value in arr:

        digit = (value // exponent) % base

        frequency_arr[digit] = frequency_arr[digit] + 1

    heads = np.zeros(base, dtype=int)

    tails = np.full(base, frequency_arr[0])

    for i in range(1, base):

        heads[i] = heads[i-1] + frequency_arr[i-1]

        tails[i] = tails[i-1] + frequency_arr[i]

    print("current exp", exponent)

    print("heads", heads)

    print("tails", tails)

    for i in range(base):

        while heads[i] < tails[i]:

            current_item = arr[heads[i]]

            while (current_item // exponent) % base != i:

                correct_bucket = (current_item // exponent) % base

                arr[heads[correct_bucket]], current_item = current_item, arr[heads[correct_bucket]]

                heads[correct_bucket] = heads[correct_bucket] + 1

            arr[heads[i]] = current_item

            heads[i] = heads[i] + 1



    print("arr",arr)

    if exponent>=1:

        left_boundary = 0

        for i in range(0, base):

            current_bucket = arr[left_boundary:tails[i]]

            print("bucket", current_bucket)

            left_boundary = tails[i]

            inplace_msd_radix_sort(current_bucket, len(current_bucket), exponent//10, base)

    return arr

array = np.array([0, 2, 1, 123, 2, 1, 2, 0, 3, 4, 0, 12, 3, 12, 2, 3, 2, 1234, 1], dtype=int)

array = np.array([1000, 9, 111, 92, 55, 202, 66, 116, 670, 15], dtype=int)

print('Input Array {0}'.format(array))

base = 10

exponent = base ** (int(len(str(max(array)))) - 1)

sorted_array = inplace_msd_radix_sort(array, len(array), exponent, base)

print('Output Array {0}'.format(sorted_array))
