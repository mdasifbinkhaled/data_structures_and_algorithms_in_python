# Python3 program for implementation of Shell Sort
import math


def shellSort(arr):
    gap = len(arr) // 2  # initialize the gap

    while gap > 0:
        i = 0
        j = gap

        # check the array in from left to right
        # till the last possible index of j
        while j < len(arr):

            if arr[i] > arr[j]:
                arr[i], arr[j] = arr[j], arr[i]

            i += 1
            j += 1

            # now, we look back from ith index to the left
            # we swap the values which are not in the right order.
            k = i
            while k - gap > -1:

                if arr[k - gap] > arr[k]:
                    arr[k - gap], arr[k] = arr[k], arr[k - gap]
                k -= 1

        gap //= 2



# This code is contributed by Shubham Prashar (SirPrashar)

def gap_sequence(n, gap_method):

    if gap_method == 'shell':

        gaps = []

        while n>1:

            n = n//2

            gaps.append(n)

        return gaps

def shell_sort(arr, n, gap_method):

    gaps = gap_sequence(n, gap_method)

    for gap in gaps:





    while gap>0:

        i = 0

        j = gap

        while j<n:

            if arr[i] > arr[j]:

                arr[i], arr[j] = arr[j], arr[i]

            i = i + 1

            j = j + 1

    for i in range(1, n):

        key = arr[i]

        j = i - 1

        while j >= 0 and arr[j] > key:

            arr[j + 1] = arr[j]

            j = j - 1

        arr[j + 1] = key

    return arr


# array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]
#
# print('Input Array {0}'.format(array))
#
# sorted_array = insertion_sort(array, len(array))
#
# print('Output Array {0}'.format(sorted_array))
