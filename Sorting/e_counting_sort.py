# reference
# http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.681.4955&rep=rep1&type=pdf

import numpy as np

def e_counting_sort(arr, n, k):

    frequency_arr = np.zeros(k, dtype=int)

    for i in range(n):

        frequency_arr[arr[i]] = frequency_arr[arr[i]] + 1

    j = 0

    for i in range(k):

        while frequency_arr[i]>0:

            arr[j] = i

            j = j + 1

            frequency_arr[i] = frequency_arr[i] - 1

    return arr

array = np.array([2, 1, 2, 1, 2,0, 3, 4, 0, 12, 3, 12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = e_counting_sort(array, len(array), max(array) + 1)

print('Output Array {0}'.format(sorted_array))
