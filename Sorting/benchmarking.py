


def call_sorting_algorithm(arr, n):
    return bubble_sort(arr, n)


def get_array(mode, lower_range, upper_range, quantity):
    if mode == 'sorted_ascending_positive_integers':

        # sorted (ascending) positive integers
        step = abs(math.floor(upper_range / quantity))

        arr = [i for i in range(0, upper_range + 1, step)]

        return arr[0:quantity], '# Sorted (Ascending) Positive Integers'

    elif mode == 'sorted_descending_positive_integers':

        # sorted (descending) positive integers

        step = abs(math.floor(upper_range / quantity))

        arr = [i for i in range(0, upper_range + 1, step)]

        return arr[0:quantity], '# Sorted (Descending) Positive Integers'

    elif mode == 'sorted_ascending_negative_integers':

        # sorted (ascending) negative integers

        step = abs(math.floor(lower_range / quantity))

        arr = [i for i in range(lower_range, 0, step)]

        return arr[0:quantity], '# Sorted (Descending) Positive Integers'


# sorting algorithm

sorting_algorithm = 'Bubble Sort'

# Sorted (Ascending) Positive Integers

arr, information = get_array('sorted_ascending_positive_integers', -1000, 1000, 30)

n = len(arr)

print('{0} {1}'.format(n, information))

print(arr)

required_time = timeit.timeit('call_sorting_algorithm(arr, n)', number=10, globals=globals()),

print('Total time required to sort {0} {1} using {2} in seconds is: {3}'.format(n,
                                                                                information,
                                                                                sorting_algorithm,
                                                                                required_time[0]))

# Sorted (Descending) Positive Integers

arr, information = get_array('sorted_descending_positive_integers', -1000, 1000, 30)

n = len(arr)

print('{0} {1}'.format(n, information))

print(arr)

required_time = timeit.timeit('call_sorting_algorithm(arr, n)', number=10, globals=globals()),

print('Total time required to sort {0} {1} using {2} in seconds is: {3}'.format(n,
                                                                                information,
                                                                                sorting_algorithm,
                                                                                required_time[0]))


def call(sorting_algorithm, mode):

    # Sorted (Descending) Negative Integers

    arr, information = get_array(mode, -1000, 1000, 30)

    n = len(arr)

    print('{0} {1}'.format(n, information))

    print(arr)

    required_time = timeit.timeit('call_sorting_algorithm(arr, n)', number=10, globals=globals()),

    print('Total time required to sort {0} {1} using {2} in seconds is: {3}'.format(n,
                                                                                    information,
                                                                                    sorting_algorithm,
                                                                                    required_time[0]))

call(sorting_algorithm, 'sorted_ascending_negative_integers')