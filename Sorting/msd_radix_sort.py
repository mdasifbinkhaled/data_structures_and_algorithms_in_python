import numpy as np

def msd_radix_sort(arr, n, exponent, base):

    buckets = [[] for _ in range(base)]

    if n <= 1:

        return arr

    for value in arr:

        digit = (value // exponent) % base

        buckets[digit].append(value)

    if exponent > 1:

        for i in range(base):

            if buckets[i] != []:

                buckets[i] = msd_radix_sort(buckets[i], len(buckets[i]), exponent // base, base)

    output_arr = np.zeros(n, dtype=int)

    index = 0

    for bucket in buckets:

        for value in bucket:

            output_arr[index] = value

            index = index + 1

    return output_arr

array = np.array([0, 2, 1, 123, 2, 1, 2, 0, 3, 4, 0, 12, 3, 12, 2, 3, 2, 1234, 1], dtype=int)

print('Input Array {0}'.format(array))

base = 10

exponent = base ** (int(len(str(max(array)))) - 1)

sorted_array = msd_radix_sort(array, len(array), exponent, base)

print('Output Array {0}'.format(sorted_array))
