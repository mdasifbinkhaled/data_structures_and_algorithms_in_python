import numpy as np

def lets_try(arr, n):

    frequency_table = {}

    for value in arr:

        if value not in frequency_table.keys():

            frequency_table[value] = 1

        else:

            frequency_table[value] = frequency_table[value] + 1

    for key, value in frequency_table.items():

        while value > 0:

            print(key, end=', ')

            value = value - 1
    print()

array = np.array([-2.8, 1, 2, 1, 2.3, 0, 3, 4, 0, -2.4, 2.3, 12, 3, 12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = lets_try(array, len(array))

print('Output Array {0}'.format(sorted_array))
