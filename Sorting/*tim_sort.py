import numpy as np

def binary_search(arr, n, value, l, r):

    while l <= r:

        mid = l + (r - l) // 2

        if value < arr[mid]:

            r = mid - 1

        else:

            l = mid + 1

    return l

def binary_insertion_sort(arr, n):

    for i in range(1, n):

        j = binary_search(arr, n, arr[i], 0, i-1)

        arr = np.concatenate([arr[:j],[arr[i]], arr[j:i], arr[i+1:]])

    return arr

array = np.array([2, 1, 2, 1, -2.3, 0.5, 3.3, 4, 0, 12, 3, -12, 2, 0, -0.3, 2], dtype=float)

print('Input Array {0}'.format(array))

sorted_array = binary_insertion_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))
