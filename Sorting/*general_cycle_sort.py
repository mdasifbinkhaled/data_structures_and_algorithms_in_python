import numpy as np

def general_cycle_sort(arr, n, k):

    output_arr = np.zeros(n, dtype=int)

    frequency_arr = np.zeros(k, dtype=int)

    for i in range(n):

        frequency_arr[arr[i]] = frequency_arr[arr[i]] + 1

    initial_cumulative_frequency_arr = np.zeros(k, dtype=int)

    initial_cumulative_frequency_arr[0] = 0

    for i in range(0, k):

        initial_cumulative_frequency_arr[i+1] = initial_cumulative_frequency_arr[i] + frequency_arr[i]

    print(frequency_arr)

    print(initial_cumulative_frequency_arr)

    for i in range(0, n):

        current_item = arr[i]

        target_position = initial_cumulative_frequency_arr[current_item]

        if i < target_position:

            continue

        while i!=target_position:

            initial_cumulative_frequency_arr[current_item] = initial_cumulative_frequency_arr[current_item] + 1








def cycle_sort(arr, n):
    writes = 0

    for i in range(0, n - 1):

        current_item = arr[i]

        target_position = i

        for j in range(i + 1, n):

            if current_item > arr[j]:
                target_position = target_position + 1

        if target_position == i:
            continue

        while current_item == arr[target_position]:
            target_position = target_position + 1

        arr[target_position], current_item = current_item, arr[target_position]

        writes = writes + 1

        while target_position != i:

            target_position = i

            for j in range(i + 1, n):

                if arr[j] < current_item:
                    target_position = target_position + 1

            while current_item == arr[target_position]:
                target_position = target_position + 1

            arr[target_position], current_item = current_item, arr[target_position]

            writes = writes + 1

    return arr


array = np.array([2, 1, 12, 2, 1, 2, 0, 3, 4, 0, 12, 3, 12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = general_cycle_sort(array, len(array), max(array) + 1)

print('Output Array {0}'.format(sorted_array))