def get_fibonacci_binet(n):

    alpha = (1 + np.sqrt(5))/2

    beta  = (1 - np.sqrt(5))/2

    sqrt_five = np.sqrt(5)

    fibonacci = np.rint((alpha ** n - beta ** n)/sqrt_five)

    return fibonacci