def merge(arr, p, q, r):

    n1 = q - p + 1

    n2 = r - q

    left_array = [0] * (n1 + 1)

    right_array = [0] * (n2 + 1)

    for i in range(0, n1):

        left_array[i] = arr[p + i]

    for i in range(0, n2):

        right_array[i] = arr[q + i + 1]

    left_array[n1] = float('inf')

    right_array[n2] = float('inf')

    i = 0

    j = 0

    for k in range(p, r + 1):

        if left_array[i] <= right_array[j]:

            arr[k] = left_array[i]

            i = i + 1

        else:

            arr[k] = right_array[j]

            j = j + 1

    return arr

def merge_sort(arr, p, r):

    if p < r:

        q = p + (r - p) // 2

        merge_sort(arr, p, q)

        merge_sort(arr, q + 1, r)

        merge(arr, p, q, r)

    return arr


array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = merge_sort(array, 0, len(array) - 1)

print('Output Array {0}'.format(sorted_array))
