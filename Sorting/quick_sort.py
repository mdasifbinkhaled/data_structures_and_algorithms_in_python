def partition(arr, l, r):

    pivot = arr[r]

    i = l - 1

    for j in range(l, r):

        if arr[j] <= pivot:

            i = i + 1

            (arr[i], arr[j]) = (arr[j], arr[i])

    (arr[i + 1], arr[r]) = (arr[r], arr[i + 1])

    return i + 1

def quick_sort(arr, l, r):

    if l < r:

        q = partition(arr, l, r)

        quick_sort(arr, l, q - 1)

        quick_sort(arr, q + 1, r)

    return arr


array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = quick_sort(array, 0, len(array) - 1)

print('Output Array {0}'.format(sorted_array))
