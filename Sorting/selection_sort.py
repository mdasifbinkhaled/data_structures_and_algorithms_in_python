def selection_sort(arr, n):

    for i in range(0, n-1):

        min_index = i

        for j in range(i+1, n):

            if arr[j] < arr[min_index]:

                min_index = j

        arr[i], arr[min_index] = arr[min_index], arr[i]

    return arr

array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = selection_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))