import numpy as np

def counting_sort_by_digit(arr, n, base, exponent):

    output_arr = np.zeros(n, dtype=int)

    frequency_arr = np.zeros(base, dtype=int)

    for i in range(n):

        index = (arr[i]//exponent) % 10

        frequency_arr[index] = frequency_arr[index] + 1

    for i in range(1, base):

        frequency_arr[i] = frequency_arr[i] + frequency_arr[i - 1]

    for i in range(n-1, -1, -1):

        index = (arr[i] // exponent) % 10

        output_arr[frequency_arr[index] - 1] = arr[i]

        frequency_arr[index] = frequency_arr[index] - 1

    return output_arr

def lsd_radix_sort(arr, n, base):

    max_abs_value = max(max(arr), min(arr), key=abs)

    exponent = 1

    while (max_abs_value/exponent)>1:

        arr = counting_sort_by_digit(arr, len(arr), base, exponent)

        exponent = exponent * 10

    return arr

array = np.array([2, 1, 123, 2, 1, 2,0, 3, 4,0, 12, 3, 12, 2, 3, 2], dtype=int)

print('Input Array {0}'.format(array))

sorted_array = lsd_radix_sort(array, len(array), 10)

print('Output Array {0}'.format(sorted_array))
