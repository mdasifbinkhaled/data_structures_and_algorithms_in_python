def bubble_sort(arr, n):

    for i in range(0, n - 1):

        for j in range(0, n - i - 1):

            if arr[j] > arr[j + 1]:

                arr[j], arr[j + 1] = arr[j + 1], arr[j]

    return arr

array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

sorted_array = bubble_sort(array, len(array))

print(sorted_array)
