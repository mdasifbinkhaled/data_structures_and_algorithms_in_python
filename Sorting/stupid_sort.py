def stupid_sort(arr, n):

    i = 0

    while i < n - 1:

        if arr[i] > arr[i+1]:

            arr[i], arr[i + 1] = arr[i + 1], arr[i]

            i = i - 1

            if i == -1:

                i = 0
        else:

            i = i + 1

    return arr

array = [2, 1, 2, 1, 2, 3, 4, 12, 3, 12, -2.1, 3.1, 2.132]

print('Input Array {0}'.format(array))

sorted_array = stupid_sort(array, len(array))

print('Output Array {0}'.format(sorted_array))